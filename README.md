# Animations #

I love creating animations in apps. I've built some animations in the Neon app and I would like to show it to you.


### Rocket Image Parallax ###
https://drive.google.com/file/d/1i8KW5jg7ZUbasmYZp5125QhOOqd8d5lQ/view?usp=sharing

I customized a View. The elements of image are separated.
When the user moves his finger down in the image, I capture the starting point. Then, when the user moves his finger, I calculate the distance between current point and the starting point, and I move the each element with a different factor (parallax effect) applied in the distance. When the user moves his finger up, I move the elements to the original position with overshoot interpolator.

### Scratch the image ###
https://drive.google.com/file/d/1y7EVmB4PR3BJk7aEAlncO0lHh3QAZgUp/view?usp=sharing

At init, I draw an image in a canvas (raspadinha) and when the user touches the image, I paint the point (finger) with transparent color (PorterDuff.Mode.CLEAR). When the user moves their finger up, I calculate the percentage that has been cleared (transparent pixels). If the percentage is greather than 80%, I fade out the image and I play a lottie.

### Tutorial Overlay ###
https://drive.google.com/file/d/1kONlqv87AqLvgUXGH9lPrtsIDdEUvB65/view?usp=sharing

At init, I create a circular reveal animation. So, I calculate the midpoint, between counter and avatar, and I expand the spotlight view (part of image is transparent to direct the focus of user) to final position. The spotlight is constructed similarly to the previous animation (PorterDuff.Mode.CLEAR).

### Rocket at top of the list (alpha version) ###
https://drive.google.com/file/d/1g69-1dex0_Fd22VJqqfQk09_uSkoaNdV/view?usp=sharing

This animation is based on the Snapchat app.
I customized a NestedScrollView. When the user swipes down the container and the rocket (lottie) is visible, I play the lottie in loop (progress between 0.0 and 0.7). When the user releases the container, I vibrate the cell phone and when the lottie is at the top, I play the final lottie animation (progress between 0.7 and 1.0).

### Fluid transition between activities (alpha version) ###
https://drive.google.com/file/d/1EFntdVbJFNdlmSIU_gdkFOfGcKjPIxOt/view?usp=sharing

When the user clicks on the list item (investimentos), I move the item to the top of the screen and push the others items (cartões, transferências, ...) to down, off the screen. I move the title, value and icon back to the final position. When all the elements of the toolbar are in the final position, I start the investment activity without animation. The list is in investment activity.

### Investment flow ###
https://drive.google.com/file/d/1bp1rXIMx0mKMiWzXTYKuZcoeSxR4zyaD/view?usp=sharing

I used the shared elements trasition between list and detail screens. I shared the card view. The card has an animation with TransitionManager. Both screens have a BottomSheet. When the user click on back button, I customized the animation of the shared elements transition to update the card view (eg. switch view).

### Personal loan - Contraction flow ###
https://drive.google.com/file/d/1qznzoYFIIp2RScqdd8t9nSE1TtqGaXaG/view?usp=sharing